# Uralochka 3

UCI-compatible chess engine using Alpha-beta search and NNUE-like neural network.

The engine implements:
- Basic control options (search to depth, time per move, time per game, time per N moves).
- Support for Syzygy endgame tables.
- Multithreading.
- Builds for different processor extensions (SSE, AVX2, AVX512) and operating systems (Windows, Linux).

Engine architecture (everything is like most engines of this level):
- Position representation and move generation - magic beatboards.
- Sorting moves: moves from the transposition table, good captures by SEE, killer moves, quiet moves by history, bad captures by SEE.
- Search - PVS, Aspiration search, Quiescence Search. And all possible abbreviations and cuts in the search (Null move, Static null move, abbreviations for quiet moves, Futility pruning, SEE prunung, LMR for quiet moves).
- Transposition table for sorting moves, cuttings and for evaluation caching.
- Multithreading - Lazy SMP.
- Evaluation - a neural network similar to NNUE of the first versions with HalfKP. Network architecture: (2x(12288x1024))x6. Input layer: KingArea*Piece*Color*PieceSquare 16*6*2*64. Trained using the frameworks Keras (early versions) and PyTorch (latest versions) on data generated in games of the engine against itself (with added positions from games against other engines) with ultra-short control (8-9 half-moves). Implementation of inference in the engine using the accumulator principle and vector instructions (SSE, AVX).
- HCE (not used now) - material, PSQT, king's pawn shield, attacks on the king, pawn structure, piece mobility. The weights were selected using the Texel engine method.

The engine uses external libraries:
- https://github.com/official-stockfish/nnue-pytorch/blob/master/feature_transformer.py - a class with the implementation of fast sparse tensors for PyTorch from the nnue-pytorch project. Using this implementation significantly accelerated network training.
- https://github.com/official-stockfish/nnue-pytorch/blob/master/feature_transformer.py - a class with the implementation of fast sparse tensors for PyTorch from the nnue-pytorch projector. Using this implementation significantly accelerated the learning of the network.
- https://github.com/jdart1/Fathom - access to Syzygy endgame tables.
- https://github.com/graphitemaster/incbin - attaching a binary file to an executable file.
- https://github.com/rogersce/cnpy - saving datasets in NumPy format.
- https://pstreams.sourceforge.net - running processes in Linux.

When creating the engine, information was also used:
- Chess programming wiki: https://www.chessprogramming.org/Main_Page
- Engines Ethereal (https://github.com/AndyGrant/Ethereal) and Igel (https://github.com/vshcherbyna/igel) - looked at the search procedure of modern engines (the search in these engines is easier to understand than in Stockfish).
- Stockfish engine (https://github.com/official-stockfish/Stockfish) and training utility (https://github.com/glinscott/nnue-pytorch) - looked at the principles of implementing a neural network and generating dataset for training.
- Koivisto engine (https://github.com/Luecx/Koivisto) - looked at the principle of using vector instructions for calculating the output layer of a neural network.
- Some other engines.

Many thanks to the authors of these engines and libraries!

I am also grateful to the entire computer chess community. And especially for people who test chess engines.

Engine History:
- I started to get interested in chess programming somewhere in 2006, I wrote the FreeChess engine, which is very weak.
- After participating in the CIS Championship in 2008 and communicating with the authors of the best CIS engines, I started a new engine, Uralochka (the name was suggested by Igor Korshunov, author of WildCat). Which played much stronger than FreeChess, but still quite weak (CCRL rating 2200).
- Around 2014, there was a not very successful attempt to write a new engine - Uralochka 2. According to my estimates, the rating was about 2400 (according to CCRL), but due to a disk failure in the laptop, the source code with the binaries was lost. It's a pity there's no more for history.
- In the spring of 2021, colleagues at work decided to compete in writing a chess engine. I decided to participate and started a new, 3rd version of Uralochka. Because was limited by the deadline of our “competition”, wrote everything in a hurry, used the 0x88 generator, search procedure and evaluation was similar to the first Uralochka. Because I was the only one who had a working engine, the competition did not take place. I decided to continue developing Uralochka (at least while there is noticeable progress). I gradually rewrote the generator to magic bitboards (but not completely, pawn moves are still generated through coordinates, not through bitboards, I'm too lazy to do it normally), implemented basic reductions/cuts in the search, complicated the evaluation, implemented tuning using the Texel engine method. In the spring of 2022, progress slowed down significantly (somewhere around 3100 according to CCRL) and I decided to dig into neural networks. After a couple of months of experimentation, I got the first version with a neural network instead of an evaluation function, which was stronger than the version with a handwritten evaluation. After several training cycles (training on data generated by the previous version) and some improvements to the neural network, the engine was put into public access.

License - GPL-3.

P.S. Sorry for the poor quality of the code. Unfortunately, I'm not a big C++ expert.
